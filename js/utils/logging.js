/* eslint import/no-extraneous-dependencies: "off" */
import Sentry from 'sentry-expo';
import { Constants } from 'expo';
import { SentrySeverity, SentryLog } from 'react-native-sentry';
import { SENTRY_DSN } from './constants';

/**
* Meego's Sentry service. This is used for remote logging.
*
*/

function Logger() { }

const l = new Logger();

const isProduction = Constants.appOwnership === 'standalone' || Constants.appOwnership === 'guest';

// Will call this on app load.
Logger.prototype.init = function () {
    Sentry.enableInExpoDevelopment = false;

    Sentry.config(SENTRY_DSN, {
        logLevel: isProduction ? SentryLog.Debug : SentryLog.Debug
    }).install();

    this.sentry = Sentry;
};

Logger.prototype.setUserContext = function (id, email, name) {
    this.sentry.setUserContext({
        email, id, name
    });
};

Logger.prototype.setEnvironment = function (tags) {
    this.sentry.setTagsContext(tags);
};

Logger.prototype.warn = function (message, functionName, eventTags = {}, err = null) {
    try {
        this.sentry.captureMessage(message, {
            level: SentrySeverity.Warning,
            extra: {
                function: functionName, extra: eventTags, err: err.toString() || '', localTime: new Date().toISOString()
            }
        });
    } catch (e) {
        console.warn('Sentry error', e);
    }
};

Logger.prototype.error = function (message, functionName, eventTags = {}, err = null) {
    console.warn(message);
    try {
        this.sentry.captureMessage(message, {
            level: SentrySeverity.Error,
            extra: {
                function: functionName, extra: eventTags, err: err.toString() || '', localTime: new Date().toISOString()
            }
        });
    } catch (e) {
        console.warn('Sentry error', e);
    }
};

Logger.prototype.exception = function (err, functionName, eventTags = {}) {
    console.warn(err);
    try {
        this.sentry.captureException(err, {
            level: SentrySeverity.Error,
            extra: {
                function: functionName, extra: eventTags, localTime: new Date().toISOString()
            }
        });
    } catch (e) {
        console.warn('Sentry error', e);
    }
};

export default l;
