import { Segment, Constants } from 'expo';
import Logger from './logging';
import { SEGMENT_IOS_KEY, SEGMENT_ANDROID_KEY } from './constants';

/**
* Meego's Segment analytics service. This is used for track and
* then user events over to segmnet which
*  in turn pipes those events to Mixpanel.
*
*/

function Events() { }

const e = new Events();

const isProduction = Constants.appOwnership === 'standalone' || Constants.appOwnership === 'guest';

Events.prototype.init = function () {
    // Only initialize segment if this is production.
    if (isProduction) {
        Segment.initialize({ androidWriteKey: SEGMENT_ANDROID_KEY, iosWriteKey: SEGMENT_IOS_KEY });
    } else {
        console.log('Not initializing analytics in development.');
    }
};

Events.prototype.setIdentity = function (email, name, gender = 'na', age = 0) {
    try {
        Segment.identifyWithTraits(email, {
            name, gender, age
        });
    } catch (err) {
        Logger.exception(err, 'events:setIdentity', {
            email, name, gender, age
        });
    }
};

Events.prototype.track = function (event, props = null) {
    try {
        if (!props) {
            Segment.track(event);
        } else {
            Segment.trackWithProperties(event, props);
        }
    } catch (err) {
        Logger.exception(err, 'events:Track', props || {});
    }
};

Events.prototype.trackScreen = function (screenName, props = null) {
    try {
        if (!props) {
            Segment.screen(screenName);
        } else {
            Segment.screenWithProperties(screenName, props);
        }
    } catch (err) {
        Logger.exception(err, 'events:trackScreen', props || {});
    }
};

export default e;
