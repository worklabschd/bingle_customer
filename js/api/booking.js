import axios from 'axios';
import { SERVER_URL } from '../utils/constants';
import { handleError } from './login';


const updateLocation = (id, latitude, longitude) => new Promise((resolve, reject) => {
    axios.post(`${SERVER_URL}/update-location`, {
        id: id || undefined,
        latitude,
        longitude
    })
        .then(response => {
            resolve(response.data);
        })
        .catch(err => {
            handleError(err, reject);
        });
});

const requestCall = (id, phone, language, latitude, longitude) => new Promise((resolve, reject) => {
    axios.get(`${SERVER_URL}/call-booking`, {
        params: {
            id,
            phone,
            language,
            latitude,
            longitude
        }
    })
        .then(response => {
            resolve(response.data);
        })
        .catch(err => {
            handleError(err, reject);
        });
});

const getCallHistory = (id, offset) => new Promise((resolve, reject) => {
    axios.get(`${SERVER_URL}/call-history`, {
        params: {
            id,
            offset
        }
    })
        .then(response => {
            resolve(response.data);
        })
        .catch(err => {
            handleError(err, reject);
        });
});


export { updateLocation, requestCall, getCallHistory };
