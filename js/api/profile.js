import axios from 'axios';
import { SERVER_URL } from '../utils/constants';
import { handleError } from './login';

const updateProfilePicture = (id, avatar) => new Promise((resolve, reject) => {
    axios.post(`${SERVER_URL}/edit-avatar`, {
        id,
        avatar
    })
        .then(response => {
            resolve(response.data);
        })
        .catch(err => {
            handleError(err, reject);
        });
});

const updateProfile = (id, profile) => new Promise((resolve, reject) => {
    axios.post(`${SERVER_URL}/edit-profile`, {
        id,
        ...profile
    })
        .then(response => {
            resolve(response.data);
        })
        .catch(err => {
            handleError(err, reject);
        });
});

export { updateProfilePicture, updateProfile };
