import axios from 'axios';
import { Constants } from '.';
import { SERVER_URL } from '../utils/constants';

const loginUser = (email, password) => new Promise((resolve, reject) => {
    axios.post(`${SERVER_URL}/login`, {
        email,
        password
    })
        .then(response => {
            resolve(response.data);
        })
        .catch(err => {
            handleError(err, reject);
        });
});

const socialLogin = (type, token) => new Promise((resolve, reject) => {
    axios.post(`${SERVER_URL}/social-login`, {
        type,
        token
    })
        .then(response => {
            resolve(response.data);
        })
        .catch(err => {
            handleError(err, reject);
        });
});

const validateEmail = email => new Promise((resolve, reject) => {
    axios.get(`${SERVER_URL}/validate-email`, {
        params: {
            email
        }
    })
        .then(() => {
            resolve();
        })
        .catch(err => {
            handleError(err, reject);
        });
});

const OTP = {
    sendOTP(countryCode, phone) {
        return new Promise((resolve, reject) => {
            axios.get(`${SERVER_URL}/send-otp`, {
                params: {
                    countryCode, phone
                }
            })
                .then(() => {
                    resolve();
                    // Saving these for verifyOTP.
                    this.countryCode = countryCode;
                    this.phone = phone;
                })
                .catch(err => {
                    handleError(err, reject);
                });
        });
    },
    verifyOTP(otp) {
        return new Promise((resolve, reject) => {
            if (!(this.countryCode && this.phone)) {
                reject(new Error('sendOTP not called first.'));
                return;
            }
            axios.get(`${SERVER_URL}/verify-otp`, {
                params: {
                    countryCode: this.countryCode.replace('+', ''), phone: this.phone, otp: otp - 0
                }
            })
                .then(response => {
                    const { data: { message, status } } = response;
                    if (message === 'OK') {
                        resolve();
                        return;
                    }
                    reject(status);
                })
                .catch(err => {
                    handleError(err, reject);
                });
        });
    },
    resendOTP() {
        return new Promise((resolve, reject) => {
            if (!(this.countryCode && this.phone)) {
                reject(new Error('sendOTP not called first.'));
                return;
            }
            axios.get(`${SERVER_URL}/resend-otp`, {
                params: {
                    countryCode: this.countryCode.replace('+', ''), phone: this.phone
                }
            })
                .then(response => {
                    const { data: { message, status } } = response;
                    if (message === 'OK') {
                        resolve();
                        return;
                    }
                    reject(status);
                })
                .catch(err => {
                    handleError(err, reject);
                });
        });
    }
};

const registerUser = (
    email,
    password,
    countryCode,
    phone,
    name
) => new Promise((resolve, reject) => {
    axios.post(`${SERVER_URL}/register`, {
        email, password, countryCode, phone, name
    })
        .then(response => {
            resolve(response.data);
        })
        .catch(err => {
            handleError(err, reject);
        });
});

const forgotPassword = email => new Promise((resolve, reject) => {
    axios.get(`${SERVER_URL}/forgot-password`, {
        params: {
            email
        }
    })
        .then(() => {
            resolve();
        })
        .catch(err => {
            handleError(err, reject);
        });
});

const handleError = (err, reject, default400 = Constants.INVALID_EMAIL) => {
    const { status, data } = err.response;
    // It's a bad request.
    if (status === 400) {
        switch (data.message) {
        case Constants.INVALID_EMAIL:
            reject(Constants.INVALID_EMAIL);
            return;
        case Constants.INVALID_PASSWORD:
            reject(Constants.INVALID_PASSWORD);
            return;
        case Constants.EMAIL_TAKEN:
            reject(Constants.EMAIL_TAKEN);
            return;
        default:
            // If the request has a malformed email.
            // This would go here.
            reject(default400);
            return;
        }
    }
    // It's an internal server error.
    if (status === 500) {
        reject(Constants.INTERNAL_SERVER_ERROR);
        return;
    }
    console.warn(err);
    // It's a connection error.
    reject(Constants.CONNECTION_ERROR);
};

export { loginUser, socialLogin, validateEmail, registerUser, OTP, forgotPassword, handleError };
