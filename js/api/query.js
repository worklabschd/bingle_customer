import axios from 'axios';
import { handleError } from './login';
import { SERVER_URL } from '../utils/constants';

const submitQuery = (id, name, phone, email, body) => new Promise((resolve, reject) => {
    axios.post(`${SERVER_URL}/query`, {
        id,
        name,
        phone,
        email,
        body
    })
        .then(response => {
            resolve(response.data);
        })
        .catch(err => {
            handleError(err, reject);
        });
});

export { submitQuery }; // eslint-disable-line
