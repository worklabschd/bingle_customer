import axios from 'axios';
import { SERVER_URL } from '../utils/constants';
import { handleError } from './login';


const appOpened = () => new Promise((resolve, reject) => {
    axios.put(`${SERVER_URL}/app-opened`)
        .then(response => {
            resolve(response.data);
        })
        .catch(err => {
            handleError(err, reject);
        });
});

export { appOpened }; // eslint-disable-line
