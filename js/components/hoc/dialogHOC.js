import React from 'react';
import PropTypes from 'prop-types';
import { View, StyleSheet } from 'react-native';
import { MeegoDialog } from '../common';
import { DialogEvent, LoadingEvent } from '../../events';

class DialogHOC extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            loadingDialogVisible: false,
            dialogVisible: false,
            dialogTitle: '',
            dialogBody: '',
            showCancel: false,
            closeText: '',
            cancelText: ''
        };
    }

    componentDidMount() {
        DialogEvent.subscribe((dialogTitle, dialogBody, closeText, showCancel, cancelText) => {
            this.setState({
                dialogVisible: true,
                dialogTitle,
                dialogBody,
                closeText,
                showCancel,
                cancelText
            });
        });
        LoadingEvent.subscribe(loadingDialogVisible => {
            this.setState({
                loadingDialogVisible
            });
        });
    }

    componentWillUnmount() {
        DialogEvent.dispose();
        LoadingEvent.dispose();
    }

    resetDialog() {
        this.setState({
            dialogVisible: false,
            dialogTitle: '',
            dialogBody: '',
            showCancel: false,
            closeText: ''
        });
    }

    render() {
        const {
            loadingDialogVisible,
            dialogVisible,
            dialogTitle,
            dialogBody,
            closeText,
            cancelText,
            showCancel
        } = this.state;
        return (
            <View style={styles.app} >
                {this.props.children}
                {!loadingDialogVisible ?
                    <MeegoDialog
                        visible={dialogVisible}
                        title={dialogTitle}
                        body={dialogBody}
                        closeText={closeText}
                        cancelText={cancelText}
                        onClose={() => {
                            DialogEvent.confirm();
                            this.resetDialog();
                        }}
                        showCancel={showCancel}
                        onCancel={() => {
                            DialogEvent.cancel();
                            this.resetDialog();
                        }}
                    />
                    : <MeegoDialog
                        visible={loadingDialogVisible}
                        title="Loading..."
                        showProgress
                        showConfirm={false}
                    />
                }
            </View>
        );
    }
}

DialogHOC.propTypes = {
    children: PropTypes.oneOfType([
        PropTypes.element,
        PropTypes.array
    ]).isRequired
};


const styles = StyleSheet.create({
    app: {
        position: 'absolute',
        top: 0,
        bottom: 0,
        left: 0,
        right: 0
    }
});

export default DialogHOC;
