/* eslint react/prop-types: "off" */

import React from 'react';
import ErrorBoundary from './errorBoundary';
import Store from './store';
import KeyboardListener from './keyboardListener';
import DialogHOC from './dialogHOC';

export default (props) => (
    <ErrorBoundary>
        <Store>
            <DialogHOC>
                <KeyboardListener>
                    {props.children}
                </KeyboardListener>
            </DialogHOC>
        </Store>
    </ErrorBoundary>
);
