import React from 'react';
import PropTypes from 'prop-types';
import { DrawerItems, SafeAreaView, NavigationActions } from 'react-navigation';
import {
    ScrollView,
    Image,
    View,
    Text,
    Dimensions,
    TouchableOpacity,
    TouchableWithoutFeedback,
} from 'react-native';
import { MaterialIcons } from '@expo/vector-icons';
import { connect } from 'react-redux';
import { removeUser } from '../../actions';
import { BASE_PATH } from '../../utils/constants';
import { font, color } from '../theme';

const { height } = Dimensions.get('window');

const Drawer = props => {
    const {
        avatar, name, phone, navigation,
        removeUserFromStore
    } = props;

    const logout = () => {
        if (name) {
            // Perform logout if user is logged in.
            removeUserFromStore();
        }
        // Go to login.
        const action = NavigationActions.reset({
            index: 0,
            actions: [
                NavigationActions.navigate({ routeName: 'Login' })
            ]
        });
        navigation.dispatch(action);
    };

    return (
        <ScrollView style={{ flex: 1 }} >
            <View style={{
                flexDirection: 'row', paddingVertical: 30, paddingHorizontal: 15, justifyContent: 'space-around'
            }}
            >
                <TouchableWithoutFeedback onPress={() => { if (name) navigation.navigate('Settings'); }} >
                    <View>
                        <Image
                            style={{
                                borderRadius: height / 17,
                                height: height / 8.5,
                                width: height / 8.5
                            }}
                            source={avatar ? { uri: BASE_PATH + avatar } : require('../../../assets/icons/avatar.png')}
                        />
                        <View
                            style={{
                                borderRadius: height / 15,
                                height: height / 25,
                                width: height / 25,
                                position: 'absolute',
                                right: 0,
                                bottom: 0,
                                justifyContent: 'center',
                                alignItems: 'center',
                                backgroundColor: 'white',
                                borderWidth: 0.5,
                                borderColor: color.grey
                            }}
                        >
                            <MaterialIcons
                                name="mode-edit"
                                size={height / 33}
                                style={{ backgroundColor: 'transparent' }}
                            />
                        </View>
                    </View>
                </TouchableWithoutFeedback>
                <View style={{ alignSelf: 'center' }} >
                    <View style={{ flexDirection: 'row' }} >
                        <Text
                            style={{
                                minWidth: 150,
                                fontFamily: font.primary_mont_medium,
                                fontSize: 16,
                                paddingHorizontal: 5,
                                paddingBottom: 2.5
                            }}
                        >
                            {name || 'Guest User'}
                        </Text>
                    </View>
                    <View style={{ flexDirection: 'row' }} >
                        <Text
                            style={{
                                paddingHorizontal: 5,
                                fontFamily: font.primary_mont_medium,
                                fontSize: 12
                            }}
                        >{phone}
                        </Text>
                    </View>
                </View>
                <View />
                <View />
            </View>
            <SafeAreaView style={{ height: height - (height / 2.8), opacity: name ? 1 : 0.25 }} forceInset={{ top: 'always', horizontal: 'never' }}>
                {!name ?
                    <TouchableOpacity style={{
                        position: 'absolute', top: 0, bottom: 0, left: 0, right: 0, zIndex: 99
                    }}
                    /> : null }
                <DrawerItems {...props} />
            </SafeAreaView>
            <View style={{ height: height / 18 }} >
                {!name ?
                    <Text style={{
                        paddingLeft: 20,
                        fontSize: 14,
                        fontFamily: font.primary_mont,
                        color: color.maroon
                    }}
                    >* Sign in to get access to all features.
                    </Text> : null
                }
            </View>
            <TouchableOpacity onPress={logout} style={{ height: height / 18 }}>
                <Text style={{

                    paddingLeft: 20,
                    fontFamily: font.primary_mont,
                    fontSize: 16
                }}
                >
                    {name ? 'Sign Out' : 'Sign In'}
                </Text>
            </TouchableOpacity>
        </ScrollView>
    );
};


Drawer.propTypes = {
    name: PropTypes.string.isRequired,
    avatar: PropTypes.string.isRequired,
    phone: PropTypes.string.isRequired,
    navigation: PropTypes.shape().isRequired,
    removeUserFromStore: PropTypes.func.isRequired
};

const mapStateToProps = ({ user }) => ({
    name: user.name,
    avatar: user.avatar,
    phone: user.phone
});

export default connect(mapStateToProps, { removeUserFromStore: removeUser })(Drawer);
