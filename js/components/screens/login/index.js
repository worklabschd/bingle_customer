import React from 'react';
import {
    View,
    Text,
    StyleSheet,
    Platform,
    TouchableOpacity,
    Image,
    ActivityIndicator,
    Dimensions,
    Animated,
    Easing,
    BackHandler
} from 'react-native';
import { Facebook, Google } from 'expo';
import PropTypes from 'prop-types';
import { SocialIcon } from 'react-native-elements';
import { Observable } from 'rxjs';
import { NavigationActions } from 'react-navigation';
import { connect } from 'react-redux';
import { setUser } from '../../../actions';
import { MeegoLoginAPI, Constants } from '../../../api';
import { IconInput, MeegoBackground } from '../../common';
import { color, font } from '../../theme';
import {
    FACEBOOK_APP_ID,
    GOOGLE_IOS_STANDALONE,
    GOOGLE_ANDROID_STANDALONE,
    GOOGLE_WEB_STANDALONE
} from '../../../utils/constants';
import { Snackbar, Utils } from '../../widgets';
import SignUp from './SignUp';
import Logger from '../../../utils/logging';
import Events from '../../../utils/events';

const { width } = Dimensions.get('window');


class Login extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            facebookLoading: false,
            googleLoading: false,
            loading: false,
            index: 0,
            email: ''
        };

        // Login handlers.
        this.facebookLogin = this.socialLogin.bind(this, 'facebook');
        this.googleLogin = this.socialLogin.bind(this, 'google');
        this.loginBind = this.login.bind(this);
        this.onForgotPasswordBind = this.onForgotPassword.bind(this);

        // Navigation handlers.
        this.onSignUpButtonPressBind = this.onSignUpButtonPress.bind(this);
        this.onSkipBind = this.onSkip.bind(this);

        // Callback from children components.
        this.onSignUpBind = this.onSignUp.bind(this);
        this.onOTPProceedBind = this.onOTPProceed.bind(this);

        // Initializing instance members.
        this.name = '';
        this.email = '';
        this.password = '';
        this.newUser = {};

        this.translate = new Animated.Value(0);
        this.signUpInterpolated = this.translate.interpolate({
            inputRange: [0, width, 2 * width],
            outputRange: [width, 0, -width]
        });
        this.OTPInterpolated = this.translate.interpolate({
            inputRange: [0, width, 2 * width],
            outputRange: [2 * width, width, 0]
        });
        this.loginInterpolated = this.translate.interpolate({
            inputRange: [0, width, 2 * width],
            outputRange: [0, -width, -2 * width]
        });
        this.OTPTransform = {
            transform: [
                {
                    translateX: this.OTPInterpolated
                }
            ]
        };
        this.signUpTransform = {
            transform: [
                {
                    translateX: this.signUpInterpolated
                }
            ]
        };
        this.loginTransform = {
            transform: [
                {
                    translateX: this.loginInterpolated
                }
            ]
        };
        this.signUpObservable = Observable.create(observer => {
            this.signUpObserver = observer;
        });
        this.OTPObservable = Observable.create(observer => {
            this.OTPObserver = observer;
        });
        const { navigate, dispatch } = props.navigation;
        this.navigate = navigate;
        this.navigationDispatch = dispatch;
    }

    componentDidMount() {
        // TODO remove this in _cwu
        BackHandler.addEventListener('hardwareBackPress', () => {
            if (this.state.index > 0) {
                this.onSkip();
                return true;
            }
            return false;
        });
    }

    onForgotPassword() {
        this.navigate('ForgotPassword');
    }

    onSignUpButtonPress() {
        // Translate to SignUp route.
        if (this.state.index === 0) {
            Animated.timing(this.translate, {
                toValue: width,
                duration: 350,
                easing: Easing.linear
            }).start();
            this.setState({
                index: this.state.index + 1
            }, () => {
                // Clearing out email and password when the user presses sign up.
                this.emailRef.setText('');
                this.passwordRef.setText('');
            });
        }
    }

    onSkip() {
        if (this.state.index === 0) {
            Events.track('Signup skipped.');
            this.pushToDashboard();
            return;
        }
        let toValue = 0;
        if (this.state.index === 2) {
            // Translate back to SignUp route.
            // Else translate back to Login route.
            toValue = width;
        }
        Animated.timing(this.translate, {
            toValue,
            duration: 350,
            easing: Easing.linear
        }).start();
        this.setState({
            index: this.state.index - 1
        }, () => {
            if (this.state.index === 0) {
                // Clearing out email and name from social login
                // and email, password and phone if entered
                // when the user translates from sign up to login.
                this.setState({ email: '' });
                this.name = '';
                this.signUpObserver.next();
            } else if (this.state.index === 1) {
                // Clearing out OTP when the user translates from OTP to sign up.
                this.OTPObserver.next();
            }
        });
    }

    onSignUp(name, email, password, countryCode, phone) {
        // Called from SignUp component.
        // Translate to OTP route.
        this.newUser = {
            name, email, password, countryCode, phone
        };
        // Animated.timing(this.translate, {
        //     toValue: 2 * width,
        //     duration: 350,
        //     easing: Easing.linear
        // }).start();
        // this.setState({
        //     index: this.state.index + 1
        // });
        this.pushToOTP(name, email, password);
    }

    onOTPProceed() {
        // Called from OTP component.
        this.registerUser();
    }

    async registerUser() {
        const {
            email, password, countryCode, phone
        } = this.newUser;
        try {
            /* const user = */
            const newUser = await MeegoLoginAPI.registerUser(
                email,
                password,
                countryCode,
                phone,
                this.name
            );

            this.props.setUser(newUser);

            this.pushToDashboard();
        } catch (err) {
            Logger.warn(
                'Couldn\'t register user.',
                'registerUser:catch',
                Object.assign({}, this.newUser, { name: this.name }),
                err
            );
            if (err === Constants.INTERNAL_SERVER_ERROR) {
                Snackbar.displayInternalServer();
                return;
            }
            Snackbar.displayNetworkError();
        }
    }

    async login() {
        if (!this.state.loading && !this.state.googleLoading && !this.state.facebookLogin) {
            if (!this.email.trim()) {
                Utils.shakeAndDisplayToast(this.emailView, 'Email cannot be empty.');
                return;
            }
            if (!this.password) {
                Utils.shakeAndDisplayToast(this.passwordView, 'Password cannot be empty.');
                return;
            }
            this.setState({
                loading: true
            });
            try {
                const user = await MeegoLoginAPI.loginUser(
                    this.email.toLowerCase().trim(),
                    this.password
                );

                this.props.setUser(user);

                this.setState({ loading: false });
                this.pushToDashboard();
            } catch (err) {
                this.setState({ loading: false });
                switch (err) {
                case Constants.INVALID_EMAIL:
                    Utils.shakeAndDisplayToast(this.emailView, 'This email does not belong to any account. Please try again.');
                    break;
                case Constants.INVALID_PASSWORD:
                    Utils.shakeAndDisplayToast(this.passwordView, 'The password entered is incorrect. Please try again.');
                    break;
                case Constants.INTERNAL_SERVER_ERROR:
                    Snackbar.displayInternalServer();
                    break;
                default:
                    Snackbar.displayNetworkError();
                }
                Logger.warn(
                    'Couldn\'t login user.',
                    'login:catch',
                    {
                        email: this.email.toLowerCase().trim(),
                        password: this.password
                    },
                    err
                );
            }
        }
    }

    socialLogin(loginType) {
        if (!this.state.facebookLoading && !this.state.googleLoading && !this.state.loading) {
            if (loginType === 'facebook') {
                this.loginWithFacebook();
                return;
            }
            this.loginWithGoogle();
        }
    }

    socialFailure(loginType) {
        if (loginType === 'facebook') {
            this.setState({
                facebookLoading: false
            });
            Snackbar.displayToast('Could not sign into Facebook. Please try again.');
        } else {
            this.setState({
                googleLoading: false
            });

            Snackbar.displayToast('Could not sign into Google. Please try again.');
        }
    }

    socialSuccess(user) {
        if (user.status === 'EXISTING') {
            Events.setIdentity(user.email, { name: user.name });

            const existingUser = { ...user };
            delete existingUser.status;

            this.props.setUser(existingUser);
            this.pushToDashboard();
            return;
        }
        // This is a new user. Push to OTP instead.
        this.name = user.name;
        this.pushToOTP(user.name, user.email, Math.random().toString());
        // this.setState({ email: user.email }, () => {
        //     this.onSignUpButtonPress();
        // });
    }

    async loginWithFacebook() {
        this.setState({
            facebookLoading: true
        });
        try {
            const { type, token } = await Facebook.logInWithReadPermissionsAsync(FACEBOOK_APP_ID, {
                permissions: ['public_profile', 'email']
            });
            if (type === 'success') {
                const user = await MeegoLoginAPI.socialLogin('facebook', token);
                this.setState({
                    facebookLoading: false
                });
                Events.track('Signed in via Facebook.');
                this.socialSuccess(user);
            } else {
                this.socialFailure('facebook');
            }
        } catch (e) {
            this.setState({
                facebookLoading: false
            });
            this.socialFailure('facebook');
            Logger.warn(
                'Couldn\'t login user into facebook.',
                'loginWithFacebook:catch',
                null,
                e
            );
        }
    }

    async loginWithGoogle() {
        this.setState({
            googleLoading: true
        });
        try {
            const result = await Google.logInAsync({
                behavior: 'web',
                androidClientId: GOOGLE_ANDROID_STANDALONE,
                iosClientId: GOOGLE_IOS_STANDALONE,
                androidStandaloneAppClientId: GOOGLE_ANDROID_STANDALONE,
                iosStandaloneAppClientId: GOOGLE_IOS_STANDALONE,
                webClientId: GOOGLE_WEB_STANDALONE,
                scopes: ['profile', 'email'/* , 'https://www.googleapis.com/auth/user.birthday.read' */],
            });

            if (result.type === 'success') {
                const user = await MeegoLoginAPI.socialLogin('google', result.accessToken);
                this.setState({
                    googleLoading: false
                });
                Events.track('Signed in via Google.');
                this.socialSuccess(user);
            } else {
                this.socialFailure('google');
            }
        } catch (e) {
            this.setState({
                googleLoading: false
            });
            this.socialFailure('google');
            Logger.warn(
                'Couldn\'t login user into google.',
                'loginWithGoogle:catch',
                null,
                e
            );
        }
    }

    pushToDashboard() {
        const action = NavigationActions.reset({
            index: 0,
            actions: [
                NavigationActions.navigate({ routeName: 'Dashboard' })
            ]
        });
        this.navigationDispatch(action);
    }

    pushToOTP(name, email, password) {
        this.navigate('OTP', { name, email, password });
    }

    render() {
        const {
            header,
            inputs,
            logoStyle,
            headerImageContainer,
            loginButton,
            skip,
            skipButton,
            signUpButton,
            buttonText,
            backButton,
            forgotContainer,
            forgotButton,
            forgotText
        } = styles;
        return (
            <MeegoBackground>
                <SignUp
                    clearObservable={this.signUpObservable}
                    email={this.state.email}
                    transform={this.signUpTransform}
                    onSubmitSignUp={this.onSignUpBind}
                />
                {/*
                  <OTP
                    clearObservable={this.OTPObservable}
                    transform={this.OTPTransform}
                    onVerify={this.onOTPProceedBind}
                  />
                */}
                <View style={header} >
                    <TouchableOpacity
                        style={skipButton}
                        onPress={this.onSkipBind}
                    >
                        {
                            this.state.index === 0 ?
                                <Text style={skip}>Skip</Text>
                                :
                                <Image resizeMode="contain" style={backButton} source={require('../../../../assets/icons/left_arrow.png')} />
                        }
                    </TouchableOpacity>
                    <TouchableOpacity
                        style={signUpButton}
                        onPress={this.onSignUpButtonPressBind}
                    >
                        <Text style={skip}>{this.state.index === 0 ? 'Signup' : ''}</Text>
                    </TouchableOpacity>
                    <View style={headerImageContainer}>
                        <View style={{ flex: 0.94 }} />
                        <Image resizeMode="contain" style={logoStyle} source={require('../../../../assets/icons/white_logo.png')} />
                        <View style={{ flex: 0.5 }} />
                    </View>
                </View>
                <Animated.View
                    style={[inputs, this.loginTransform]}
                >
                    <IconInput
                        ref={component => { this.emailRef = component; }}
                        viewRef={component => { this.emailView = component; }}
                        image={
                            <Image
                                source={require('../../../../assets/icons/envelope_white.png')}
                                resizeMode="contain"
                                style={{ flex: 1, width: 22 }}
                            />
                        }
                        placeholder="Email"
                        inputType="email-address"
                        onChangeText={value => { this.email = value; }}
                        onSubmit={() => { this.passwordInput.focus(); }}
                    />
                    <IconInput
                        ref={component => { this.passwordRef = component; }}
                        viewRef={component => { this.passwordView = component; }}
                        inputRef={component => { this.passwordInput = component; }}
                        image={
                            <Image
                                source={require('../../../../assets/icons/unlocked_white.png')}
                                resizeMode="contain"
                                style={{ flex: 1, width: 22 }}
                            />
                        }
                        placeholder="Password"
                        isPassword
                        onChangeText={value => { this.password = value; }}
                    />
                    <View style={forgotContainer}>
                        <View style={{ flex: 1 }} />
                        <TouchableOpacity
                            style={forgotButton}
                            onPress={this.onForgotPasswordBind}
                        >
                            <Text style={forgotText}>
                              Forgot Password?
                            </Text>
                        </TouchableOpacity>
                        <View style={{ flex: 0.06 }} />
                    </View>
                </Animated.View>
                <Animated.View
                    style={[{ flex: 1.48 }, this.loginTransform]}
                >
                    <View style={{ flexDirection: 'row', flex: 1 }}>
                        <View style={{ flex: 0.02 }} />
                        <View style={{ flex: 1 }}>
                            <View style={{ flex: 0.57 }} />
                            <TouchableOpacity
                                style={loginButton}
                                onPress={this.loginBind}
                            >
                                {this.state.loading ?
                                    <ActivityIndicator color="black" />
                                    :
                                    <Text
                                        style={buttonText}
                                    >
                                  Login
                                    </Text>
                                }
                            </TouchableOpacity>
                            <View style={{ flex: 0.2 }} />
                            <TouchableOpacity onPress={this.googleLogin}>
                                <SocialIcon
                                    raised={false}
                                    button
                                    loading={this.state.googleLoading}
                                    iconStyle={this.state.googleLoading ? { width: 0 } : { }}
                                    style={{ borderRadius: 0, backgroundColor: color.google }}
                                    fontStyle={buttonText}
                                    title={this.state.googleLoading ? ' ' : 'Google Login'}
                                    type="google"
                                />
                            </TouchableOpacity>
                            <TouchableOpacity onPress={this.facebookLogin}>
                                <SocialIcon
                                    raised={false}
                                    button
                                    loading={this.state.facebookLoading}
                                    iconStyle={this.state.facebookLoading ? { width: 0 } : { }}
                                    style={{ borderRadius: 0, backgroundColor: color.facebook }}
                                    fontFamily={font.primary}
                                    fontStyle={buttonText}
                                    title={this.state.facebookLoading ? ' ' : 'Facebook Login'}
                                    type="facebook"
                                />
                            </TouchableOpacity>
                            <View style={{ flex: 0.85 }} />
                        </View>
                        <View style={{ flex: 0.02 }} />
                    </View>
                </Animated.View>
            </MeegoBackground>
        );
    }
}

Login.propTypes = {
    setUser: PropTypes.func.isRequired,
    navigation: PropTypes.shape({

    }).isRequired
};

const styles = StyleSheet.create({
    header: {
        flex: 1.5,
        flexDirection: 'column',
        alignItems: 'center'
    },
    headerImageContainer: {
        flex: 1.6,
        flexDirection: 'column'
    },
    headerText: {
        fontSize: 50,
        fontFamily: font.primary
    },
    inputs: {
        flex: 1,
        flexDirection: 'column',
        alignItems: 'center',
        justifyContent: 'center'
    },
    loginButton: {
        borderRadius: 1,
        flex: Platform.OS === 'android' ? 1.5 : 1.2,
        backgroundColor: 'white',
        alignItems: 'center',
        justifyContent: 'center',
        marginRight: 8.5,
        marginLeft: 8.5
    },
    logoStyle: {
        flex: 1,
        width: 130
    },
    skip: {
        color: 'white',
        fontFamily: font.primary,
        fontSize: 18
    },
    skipButton: {
        flex: 0.4,
        backgroundColor: '#00000000',
        position: 'absolute',
        paddingLeft: 20.5,
        paddingTop: 30,
        left: 0,
        top: 20
    },
    signUpButton: {
        flex: 0.4,
        backgroundColor: '#00000000',
        position: 'absolute',
        paddingRight: 20.5,
        paddingTop: 30,
        right: 0,
        top: 20
    },
    buttonText: {
        fontFamily: font.primary,
        fontSize: 20
    },
    backButton: {
        width: 30,
        height: 30
    },
    forgotContainer: {
        flexDirection: 'row',
        position: 'absolute',
        bottom: 0
    },
    forgotButton: {
        paddingTop: 10,
        paddingBottom: 5
    },
    forgotText: {
        alignSelf: 'flex-end',
        color: 'white',
        fontFamily: font.primary
    }
});

export default connect(null, { setUser })(Login);
