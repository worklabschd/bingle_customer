import React from 'react';
import PropTypes from 'prop-types';
import {
    View,
    Text,
    StyleSheet,
    Platform,
    TouchableOpacity,
    Image,
    ActivityIndicator,
    Animated,
    KeyboardAvoidingView
} from 'react-native';
import { IconInput } from '../../common';
import { font } from '../../theme';
import { MeegoLoginAPI, Constants } from '../../../api';
import { Snackbar, Utils } from '../../widgets';
import Logger from '../../../utils/logging';

class SignUp extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            loading: false
        };
        this.name = '';
        this.email = '';
        this.password = '';
        this.onSubmitBind = this.onSubmit.bind(this);
    }

    componentDidMount() {
        if (this.props.clearObservable) {
            this.clearDisposable = this.props.clearObservable.subscribe(() => {
                this.email = '';
                this.name = '';
                this.password = '';
                this.emailRef.setText('');
                this.passwordRef.setText('');
                this.nameRef.setText('');
                this.setState({
                    loading: false
                });
            });
        }
    }

    componentWillUnmount() {
        if (this.props.clearObservable) {
            this.clearDisposable.unsubscribe();
        }
    }

    async onSubmit() {
        if (this.state.loading) {
            return;
        }
        if (!this.name) {
            Utils.shakeAndDisplayToast(this.nameView, 'Name cannot be empty.');
            return;
        }
        if (!this.email.trim() && !this.props.email) {
            Utils.shakeAndDisplayToast(this.emailView, 'Email cannot be empty.');
            return;
        }
        if (!this.password) {
            Utils.shakeAndDisplayToast(this.passwordView, 'Password cannot be empty.');
            return;
        }
        this.setState({
            loading: true
        });
        try {
            // It's either the email entered or the one acquired from props.
            const email = this.email || this.props.email;
            // Validate uniqueness of email.
            await MeegoLoginAPI.validateEmail(email.toLowerCase().trim());
            // Send OTP to user.
            // await MeegoLoginAPI.OTP.sendOTP(this.countryCode.trim(), this.phone.trim());

            // Only Submit if user is seeing the progress bar and is not on the login screen.
            if (this.state.loading) {
                this.props.onSubmitSignUp(
                    this.name.trim(),
                    email.toLowerCase().trim(),
                    this.password
                );
                this.setState({
                    loading: false
                });
            }
        } catch (err) {
            const email = this.email || this.props.email;

            Logger.warn(
                'Couldn\'t validate email.',
                'onSubmit:catch',
                {
                    email: email.toLowerCase().trim(),
                    name: this.name.trim()
                },
                err
            );
            this.setState({
                loading: false
            });
            if (err === Constants.INVALID_EMAIL) {
                Utils.shakeAndDisplayToast(this.emailView, 'That email is invalid. Please try again.');
                return;
            }
            if (err === Constants.EMAIL_TAKEN) {
                Utils.shakeAndDisplayToast(this.emailView, 'That email is already taken. Please try again.');
                return;
            }
            if (err === Constants.INTERNAL_SERVER_ERROR) {
                Snackbar.displayInternalServer();
                return;
            }
            Snackbar.displayNetworkError();
        }
    }

    render() {
        const {
            header,
            loginButton,
            buttonText
        } = styles;
        return (
            <Animated.View
                style={[{
                    position: 'absolute', top: 0, bottom: 0, right: 0, left: 0
                }, this.props.transform]}
            >
                <View style={header} />
                <KeyboardAvoidingView
                    behavior="padding"
                    style={{
                        flexDirection: 'column', flex: 1.6, justifyContent: 'space-between'
                    }}
                >
                    <IconInput
                        ref={component => { this.nameRef = component; }}
                        viewRef={component => { this.nameView = component; }}
                        image={
                            <Image
                                source={require('../../../../assets/icons/envelope_white.png')}
                                resizeMode="contain"
                                style={{ flex: 1, width: 22 }}
                            />
                        }
                        placeholder="Enter Name"
                        editable={!this.state.loading}
                        onChangeText={value => { this.name = value; }}
                        onSubmit={() => this.emailInput.focus()}
                    />
                    <IconInput
                        ref={component => { this.emailRef = component; }}
                        viewRef={component => { this.emailView = component; }}
                        inputRef={component => { this.emailInput = component; }}
                        image={
                            <Image
                                source={require('../../../../assets/icons/envelope_white.png')}
                                resizeMode="contain"
                                style={{ flex: 1, width: 22 }}
                            />
                        }
                        placeholder={this.props.email || 'Choose Email'}
                        editable={!this.state.loading}
                        inputType="email-address"
                        onChangeText={value => { this.email = value; }}
                        onSubmit={() => this.passwordInput.focus()}
                    />
                    <IconInput
                        ref={component => { this.passwordRef = component; }}
                        viewRef={component => { this.passwordView = component; }}
                        inputRef={component => { this.passwordInput = component; }}
                        image={
                            <Image
                                source={require('../../../../assets/icons/unlocked_white.png')}
                                resizeMode="contain"
                                style={{ flex: 1, width: 22 }}
                            />
                        }
                        placeholder="Choose Password"
                        editable={!this.state.loading}
                        isPassword
                        onChangeText={value => { this.password = value; }}
                    />
                </KeyboardAvoidingView>
                <View style={{ flex: 0.9, flexDirection: 'row' }} >
                    <View style={{ flex: 0.02 }} />
                    <View style={{ flex: 1, flexDirection: 'column' }}>
                        <View style={{ flex: 1.4 }} />
                        <TouchableOpacity
                            style={loginButton}
                            onPress={this.onSubmitBind}
                        >
                            {this.state.loading ?
                                <ActivityIndicator color="black" />
                                :
                                <Text
                                    style={buttonText}
                                >
                                Continue
                                </Text>
                            }
                        </TouchableOpacity>
                        <View style={{ flex: 1.05 }} />
                    </View>
                    <View style={{ flex: 0.02 }} />
                </View>
            </Animated.View>
        );
    }
}

SignUp.propTypes = {
    onSubmitSignUp: PropTypes.func.isRequired,
    transform: PropTypes.shape({
        transform: PropTypes.array
    }),
    email: PropTypes.string,
    clearObservable: PropTypes.shape({
        subscribe: PropTypes.func
    })
};

SignUp.defaultProps = {
    transform: { transform: [] },
    email: '',
    clearObservable: null
};

const styles = StyleSheet.create({
    header: {
        flex: 1.5,
        flexDirection: 'column',
        alignItems: 'center'
    },
    loginButton: {
        borderRadius: 1,
        flex: Platform.OS === 'android' ? 1.5 : 1.2,
        backgroundColor: 'white',
        alignItems: 'center',
        justifyContent: 'center',
        marginRight: 8.5,
        marginLeft: 8.5
    },
    buttonText: {
        fontFamily: font.primary,
        fontSize: 20
    }
});

export default SignUp;
