import { StackNavigator, DrawerNavigator } from 'react-navigation';
import { Dimensions } from 'react-native';
import Login from './login';
import OTP from './otp';
import Dashboard from './dashboard';
import Profile from './profile';
import Drawer from './Drawer';
import Contact from './contact';
import Legal from './legal';
import { FAQ, FAQDetail } from './faq';
import Payment from '../screens/payment';
import AddPayment from '../screens/payment/updateAccount';
import AddAccount from '../screens/payment/addAcount';
import { BookingList, BookingDetails } from './booking';
import ExploreNearby from './exploreNearby';
import ForgotPassword from './forgotPassword';
import { font } from '../theme';
import Events from '../../utils/events';

const { width, height } = Dimensions.get('window');
const drawer = DrawerNavigator(
    {
        Dashboard: {
            screen: Dashboard
        },
        Payments: {
            screen: Payment
        },
        History: {
            screen: BookingList
        },
        Settings: {
            screen: Profile
        },
        FAQ: {
            screen: FAQ
        },
        Legal: {
            screen: Legal
        },
        Contact: {
            screen: Contact
        },
    },
    {
        initialRouteName: 'Dashboard',
        navigationOptions: {
            header: null
        },
        useNativeAnimations: true,
        drawerBackgroundColor: 'rgba(255,255,255,1)',
        activeBackgroundColor: 'rgba(255,255,255,1)',
        contentComponent: Drawer,
        drawerWidth: width - (width / 5),
        contentOptions: {
            onItemPress: route => {
                // console.log('xxx', route);
                Events.trackScreen(route);
            },
            itemsContainerStyle: { margin: 0, padding: 0 },
            itemStyle: { height: height / 15 },
            labelStyle: {
                color: '#000000', fontFamily: font.primary_mont, fontWeight: 'normal', paddingHorizontal: 5, fontSize: 16
            },
        }
    }
);

export default StackNavigator(
    {
        Login: {
            screen: Login,
        },
        OTP: {
            screen: OTP,
        },
        Dashboard: {
            screen: drawer
        },
        BookingDetails: {
            screen: BookingDetails
        },
        FAQDetail: {
            screen: FAQDetail
        },
        AddPayment: {
            screen: AddPayment
        },
        AddAccount: {
            screen: AddAccount
        },
        ExploreNearby: {
            screen: ExploreNearby
        },
        ForgotPassword: {
            screen: ForgotPassword
        }
    },
    {
        initialRouteName: 'Dashboard',
        navigationOptions: {
            header: null
        }
    }
);
