import React from 'react';
import {
    View,
    Text,
    StyleSheet,
    Platform,
    TouchableOpacity,
    Image,
    ActivityIndicator,
    BackHandler
} from 'react-native';
import * as Animatable from 'react-native-animatable';
import PropTypes from 'prop-types';
import { NavigationActions } from 'react-navigation';
import { connect } from 'react-redux';
import { setUser } from '../../../actions';
import { IconInput, MeegoBackground } from '../../common';
import { font } from '../../theme';
import { MeegoLoginAPI, Constants } from '../../../api';
import { Snackbar, Utils } from '../../widgets';
import Logger from '../../../utils/logging';
import Events from '../../../utils/events';

class OTP extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            loading: false,
            showOTP: false
        };
        this.countryCode = '' || '+91';
        this.phone = '';
        this.otp = '';
        this.onProceedBind = this.onProceed.bind(this);
        this.onResendBind = this.onResend.bind(this);
        this.onBackPressBind = this.onBackPress.bind(this);
        const { goBack, dispatch, state: { params } } = props.navigation;
        this.goBack = goBack;
        this.navigationDispatch = dispatch;
        this.navigationParams = params;
        this.hardwareBackPress = () => {
            if (this.state.showOTP) {
                this.fadeInPhone();
                return true;
            }
            return false;
        };
    }

    componentDidMount() {
        BackHandler.addEventListener('hardwareBackPress', this.hardwareBackPress);
    }

    componentWillUnmount() {
        BackHandler.removeEventListener('hardwareBackPress', this.hardwareBackPress);
    }

    // Let's send the OTP here.
    async onSend() {
        if (!this.phone) {
            Utils.shakeAndDisplayToast(this.phoneView, 'Phone cannot be empty. Please try again.');
            return;
        }
        if (this.phone.length < 10) {
            Utils.shakeAndDisplayToast(this.phoneView, 'Phone number not valid. Please try again.');
            return;
        }
        this.setState({
            loading: true
        });
        try {
            await MeegoLoginAPI.OTP.sendOTP(this.countryCode.trim(), this.phone.trim());
            this.setState({
                loading: false
            });
            this.fadeInOTP();
        } catch (err) {
            Logger.warn(
                'Couldn\'t send otp.',
                'onSend:catch',
                {
                    countryCode: this.countryCode.trim(),
                    phone: this.phone.trim()
                },
                err
            );
            this.setState({
                loading: false
            });
            if (err === Constants.INTERNAL_SERVER_ERROR) {
                Snackbar.displayInternalServer();
                return;
            }
            Snackbar.displayNetworkError();
        }
    }

    // Let's verfiy the OTP here.
    async onVerify() {
        if (!this.otp) {
            Utils.shakeAndDisplayToast(this.otpView, 'OTP cannot be empty. Please try again.');
            return;
        }
        this.setState({
            loading: true
        });
        try {
            await MeegoLoginAPI.OTP.verifyOTP(this.otp.trim());
            // Register the user after the OTP has been verified.
            const { name, email, password } = this.navigationParams;


            const newUser = await MeegoLoginAPI.registerUser(
                email,
                password,
                this.countryCode,
                this.phone,
                name
            );

            Events.setIdentity(email, {
                name
            });
            Events.track('User registered', { email });

            Logger.setUserContext(newUser.id, email, name);

            this.setState({
                loading: false
            });

            this.props.setUser(newUser);

            this.pushToDashboard();
        } catch (err) {
            this.setState({
                loading: false
            });
            switch (err) {
            case Constants.OTP_INVALID:
                Utils.shakeAndDisplayToast(this.otpView, 'Entered OTP is invalid. Please try again.');
                break;
            case Constants.OTP_EXPIRED:
                Utils.shakeAndDisplayToast(this.otpView, 'Entered OTP has expired. Please go back and try again.');
                break;
            case Constants.MOBILE_NOT_FOUND:
                Snackbar.displayToast('Entered phone number not found. Please change the number and try again.');
                break;
            case Constants.INVALID_MOBILE:
                Snackbar.displayToast('Entered phone number is invalid found. Please change the number and try again.');
                break;
            case Constants.INTERNAL_SERVER_ERROR:
                Snackbar.displayInternalServer();
                break;
            default:
                Snackbar.displayNetworkError();
            }
            Logger.warn(
                'Couldn\'t validate otp or register user.',
                'onSubmit:catch',
                { otp: this.otp.trim() },
                err
            );
        }
    }

    async onResend() {
        try {
            await MeegoLoginAPI.OTP.resendOTP();
            Snackbar.displayToast('OTP has been successfully resent.');
        } catch (err) {
            Logger.warn(
                'Couldn\'t resend otp.',
                'onResend:catch',
                {
                    countryCode: this.countryCode.trim(),
                    phone: this.phone.trim()
                },
                err
            );
            this.setState({
                loading: false
            });
            if (err === Constants.INTERNAL_SERVER_ERROR) {
                Snackbar.displayInternalServer();
                return;
            }
            Snackbar.displayNetworkError();
        }
    }

    onBackPress() {
        if (this.state.showOTP) {
            this.fadeInPhone();
            return;
        }
        this.goBack();
    }

    onProceed() {
        if (this.state.loading) {
            return;
        }
        if (!this.state.showOTP) {
            this.onSend();
            return;
        }
        this.onVerify();
    }

    pushToDashboard() {
        const action = NavigationActions.reset({
            index: 0,
            actions: [
                NavigationActions.navigate({ routeName: 'Dashboard' })
            ]
        });
        this.navigationDispatch(action);
    }

    fadeInOTP() {
        this.otp = '';
        this.phoneRef.fadeOut(300).then(() => {
            this.setState({
                showOTP: true
            }, () => {
                this.otpView.fadeIn(350);
            });
        });
    }

    fadeInPhone() {
        this.phone = '';
        this.otpView.fadeOut(300).then(() => {
            this.setState({
                showOTP: false
            }, () => {
                this.phoneRef.fadeIn(350);
            });
        });
    }

    render() {
        const {
            header,
            loginButton,
            buttonText,
            backButton,
            logoStyle,
            skipButton,
            headerImageContainer,
            footer,
            resendButton,
            resendText
        } = styles;
        return (
            <MeegoBackground>
                <View style={header} >
                    <TouchableOpacity
                        style={skipButton}
                        onPress={this.onBackPressBind}
                    >
                        <Image resizeMode="contain" style={backButton} source={require('../../../../assets/icons/left_arrow.png')} />
                    </TouchableOpacity>
                    <View style={headerImageContainer}>
                        <View style={{ flex: 0.94 }} />
                        <Image resizeMode="contain" style={logoStyle} source={require('../../../../assets/icons/white_logo.png')} />
                        <View style={{ flex: 0.5 }} />
                    </View>
                </View>
                <View
                    style={{
                        flex: 1
                    }}
                >
                    <View style={{ flex: 1 }} />
                    { !this.state.showOTP ?
                        <Animatable.View
                            ref={component => { this.phoneRef = component; }}
                            style={{
                                flex: 1, flexDirection: 'row', marginLeft: 15, marginRight: 5
                            }}
                        >
                            <View style={{ flex: 0.2 }} >
                                <IconInput
                                    image={null}
                                    placeholder="+91"
                                    inputType="phone-pad"
                                    onChangeText={value => { this.countryCode = value || '+91'; }}
                                    onSubmit={() => this.phoneInput.focus()}
                                />
                            </View>
                            <IconInput
                                viewRef={component => { this.phoneView = component; }}
                                inputRef={component => { this.phoneInput = component; }}
                                image={
                                    <Image
                                        source={require('../../../../assets/icons/telephone.png')}
                                        resizeMode="contain"
                                        style={{ flex: 1, width: 22 }}
                                    />
                                }
                                placeholder="Mobile"
                                inputType="phone-pad"
                                onChangeText={value => { this.phone = value; }}
                            />
                        </Animatable.View> :
                        <IconInput
                            viewRef={component => { this.otpView = component; }}
                            image={
                                <Image
                                    source={require('../../../../assets/icons/unlocked_white.png')}
                                    resizeMode="contain"
                                    style={{ flex: 1, width: 22 }}
                                />
                            }
                            placeholder="OTP"
                            inputType="phone-pad"
                            onChangeText={value => { this.otp = value; }}
                        />
                    }
                </View>
                <View style={{ flex: 1.48, flexDirection: 'row' }} >
                    <View style={{ flex: 0.02 }} />
                    <View style={{ flex: 1, flexDirection: 'column' }}>
                        <View style={{ flex: 3.2 }} />
                        <TouchableOpacity
                            style={loginButton}
                            onPress={this.onProceedBind}
                        >
                            {this.state.loading ?
                                <ActivityIndicator color="black" />
                                :
                                <Text
                                    style={buttonText}
                                >
                                    { this.state.showOTP ? 'Complete' : 'Verify' }
                                </Text>
                            }
                        </TouchableOpacity>
                        <View style={footer} >
                            {this.state.showOTP ?
                                <TouchableOpacity onPress={this.onResendBind} style={resendButton}>
                                    <Text style={resendText}>Complete using call</Text>
                                </TouchableOpacity>
                                : null
                            }
                        </View>
                    </View>
                    <View style={{ flex: 0.02 }} />
                </View>
            </MeegoBackground>
        );
    }
}

OTP.propTypes = {
    setUser: PropTypes.func.isRequired,
    navigation: PropTypes.shape({

    }).isRequired
};

const styles = StyleSheet.create({
    header: {
        flex: 1.5,
        flexDirection: 'column',
        alignItems: 'center'
    },
    loginButton: {
        borderRadius: 1,
        flex: Platform.OS === 'android' ? 1.5 : 1.2,
        backgroundColor: 'white',
        alignItems: 'center',
        justifyContent: 'center',
        marginRight: 8.5,
        marginLeft: 8.5
    },
    buttonText: {
        fontFamily: font.primary,
        fontSize: 20
    },
    backButton: {
        width: 30,
        height: 30
    },
    logoStyle: {
        flex: 1,
        width: 130
    },
    skipButton: {
        flex: 0.4,
        backgroundColor: '#00000000',
        position: 'absolute',
        paddingLeft: 20.5,
        paddingTop: 30,
        left: 0,
        top: 20
    },
    headerImageContainer: {
        flex: 1.6,
        flexDirection: 'column'
    },
    footer: {
        flex: 1,
        backgroundColor: '#00000000',
        marginRight: 8.5,
        marginLeft: 8.5,
        alignItems: 'flex-end'
    },
    resendButton: {
        flex: 1,
        justifyContent: 'center'
    },
    resendText: {
        fontFamily: font.primary,
        color: 'white'
    }
});

export default connect(null, { setUser })(OTP);
