import React from 'react';
import PropTypes from 'prop-types';
import { View, Text, Dimensions, Image, StyleSheet, ScrollView, Platform } from 'react-native';
import AnimatedTabs from 'react-native-animated-tabs';
import { HamContainer } from '../../common';
import { color, font } from '../../theme';
import { BASE_PATH } from '../../../utils/constants';

const { height, width } = Dimensions.get('window');

const Tab = ({
    name,
    description,
    distance,
    image
}) => (
    <View
        key={name}
        style={styles.container}
    >
        <View style={styles.imageContainer} >
            <Image
                source={{ uri: `${BASE_PATH}${image}` }}
                style={{
                    height: null,
                    width: null,
                    flex: 1,
                    borderTopLeftRadius: Platform.OS === 'android' ? height / 20 : 0,
                    borderTopRightRadius: Platform.OS === 'android' ? height / 20 : 0,
                }}
            />
        </View>
        <View style={styles.descriptionContainer}>
            <View style={styles.nameContainer}>
                <Text style={styles.name}>{name}</Text>
                <View style={{ flex: 1 }} />
                <Text style={[styles.name, { fontFamily: font.primary_mont_regular }]}>
                    {distance}
                </Text>
            </View>
            <ScrollView>
                <Text numberOfLines={10} style={styles.description}>
                    {description}
                </Text>
            </ScrollView>
        </View>
    </View>
);

const exploreNearby = ({ navigation: { goBack, state: { params: { places, placeIndex } } } }) => (
    <HamContainer
        title="Explore Nearby"
        backEnabled
        navigate={goBack}
    >
        <AnimatedTabs
            sidePanelOpacity={0.5}
            panelWidth={width / 1.3}
            sidePanelScale={0.9}
            activePanel={placeIndex}
            panelStyle={{ paddingTop: height / 30 }}
        >
            {places.map(Tab)}
        </AnimatedTabs>
    </HamContainer>
);

Tab.propTypes = {
    name: PropTypes.string.isRequired,
    description: PropTypes.string.isRequired,
    distance: PropTypes.string.isRequired,
    image: PropTypes.string.isRequired
};

exploreNearby.propTypes = {
    navigation: PropTypes.shape({
        goBack: PropTypes.func,
        state: PropTypes.shape({
            params: PropTypes.shape({})
        })
    }).isRequired
};

const styles = StyleSheet.create({
    container: {
        height: height / 1.26,
        width: width / 1.3,
        alignSelf: 'center',
        flexDirection: 'column',
        borderWidth: 1,
        borderColor: color.grey,
        borderRadius: height / 20,
        backgroundColor: color.lightGrey
    },
    imageContainer: {
        flex: 1.5,
        borderTopLeftRadius: height / 20,
        borderTopRightRadius: height / 20,
        overflow: 'hidden'
    },
    descriptionContainer: {
        flex: 1.1,
        borderBottomLeftRadius: height / 20,
        borderBottomRightRadius: height / 20,
        padding: 15
    },
    nameContainer: {
        flexDirection: 'row',
        paddingBottom: 5
    },
    name: {
        fontFamily: font.primary_mont_medium,
        fontSize: 18
    },
    description: {
        fontFamily: font.primary_mont,
        fontSize: 13,

    }
});

export default exploreNearby;
