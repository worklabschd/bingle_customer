import React, { Component } from 'react';
import PropTypes from 'prop-types';
import {
    View,
    StyleSheet,
    Dimensions,
    Text,
    Animated,
    Image,
    Easing,
    Platform
} from 'react-native';
import { MapView } from 'expo';
import * as _ from 'lodash';
import { color, font } from '../../theme';
import { HamContainer } from '../../common';
import { BASE_PATH } from '../../../utils/constants';

const { height, width } = Dimensions.get('window');
const ASPECT_RATIO = width / height;
const LATITUDE_DELTA = 0.0195;
const LONGITUDE_DELTA = LATITUDE_DELTA * ASPECT_RATIO;

class BookingDetails extends Component {
    constructor(props) {
        super(props);
        const {
            params: {
                fare,
                duration,
                time,
                avatar,
                guide,
                language,
                startLocation,
                endLocation,
                latitude,
                longitude
            }
        } = props.navigation.state;
        this.state = {
            fare,
            duration,
            time,
            avatar,
            guide,
            language: _.upperFirst(language),
            startLocation,
            endLocation,
            latitude,
            longitude
        };
        this.slideUp = new Animated.Value(0);
    }

    componentDidMount() {
        Animated.timing(this.slideUp, {
            toValue: 1.2,
            duration: 300,
            easing: Easing.linear
        }).start();
        setTimeout(() => {
            if (Platform.OS === 'ios') {
                const { latitude, longitude } = this.state;
                this.animateMapToRegion(latitude, longitude);
            }
        }, 1000);
    }

    animateMapToRegion(latitude, longitude) {
        this.map.animateToRegion({
            latitude,
            longitude,
            latitudeDelta: LATITUDE_DELTA,
            longitudeDelta: LONGITUDE_DELTA
        }, 1500);
    }

    render() {
        const {
            details,
            innerDetails,
            guide,
            guideImage,
            name,
            detailsText,
            time,
            fare
        } = styles;
        return (
            <HamContainer
                title="Tour Details"
                backEnabled
                navigate={this.props.navigation.goBack}
            >
                <MapView
                    ref={map => { this.map = map; }}
                    provider="google"
                    style={{ flex: 1 }}
                    loadingIndicatorColor={color.maroon}
                    initialRegion={{
                        latitude: this.state.latitude,
                        longitude: this.state.longitude,
                        latitudeDelta: LATITUDE_DELTA,
                        longitudeDelta: LONGITUDE_DELTA,
                    }}
                >
                    {Platform.OS === 'ios' ?
                        <MapView.Marker
                            coordinate={{
                                latitude: this.state.latitude,
                                longitude: this.state.longitude
                            }}
                            image={require('../../../../assets/icons/own_location_small.png')}
                        />
                        :
                        <MapView.Marker
                            coordinate={{
                                latitude: this.state.latitude,
                                longitude: this.state.longitude
                            }}
                            image={require('../../../../assets/icons/own_location.png')}
                        />
                    }
                </MapView>
                <Animated.View style={[{ flex: this.slideUp }, details]}>
                    <View style={innerDetails}>
                        <View style={guide}>
                            <Image
                                style={guideImage}
                                source={this.state.avatar ? { uri: BASE_PATH + this.state.avatar } : require('../../../../assets/icons/avatar.png')}
                            />
                        </View>
                        <View>
                            <Text style={name}>{this.state.guide}</Text>
                            <Text style={name}>3.0</Text>
                            <Text style={time}>{this.state.time}</Text>
                            <Text style={fare}>{this.state.fare}</Text>
                        </View>
                        <Text style={detailsText}>Duration: {this.state.duration}</Text>
                        <Text style={detailsText}>Language: {this.state.language}</Text>
                        <Text style={detailsText}>Start Location: {this.state.startLocation}</Text>
                        <Text style={detailsText}>End Location: {this.state.endLocation}</Text>
                    </View>
                </Animated.View>
            </HamContainer>
        );
    }
}

BookingDetails.propTypes = {
    navigation: PropTypes.shape({
        navigate: PropTypes.func,
        goBack: PropTypes.func,
        state: PropTypes.shape({
            params: PropTypes.shape({})
        })
    }).isRequired
};

const styles = StyleSheet.create({
    container: {
        flex: 1,
        backgroundColor: 'white'
    },
    ham: {
        top: height / 17,
        left: width / 15,
        width: 40,
        backgroundColor: 'transparent'
    },
    heading: {
        alignSelf: 'center',
        fontFamily: font.primary_mont,
        fontSize: 17,
        marginTop: 7.5,
        top: height / 17
    },
    details: {
        marginLeft: width / 17,
        marginRight: width / 17
    },
    innerDetails: {
        flex: 1,
        justifyContent: 'space-around',
        transform: [{ translateY: -height / 16.5 }]
    },
    guideImage: {
        width: width / 3.2,
        height: width / 3.2,
        borderRadius: width / 6.4
    },
    guide: {
        width: width / 3.2,
        height: width / 3.2,
        borderRadius: width / 6.4,
        elevation: 2,
        shadowColor: color.grey,
        shadowOffset: { width: 0, height: 10 },
        shadowOpacity: 1,
        shadowRadius: 10
    },
    name: {
        backgroundColor: 'transparent',
        fontFamily: font.primary_mont_medium,
        fontSize: 15
    },
    detailsText: {
        fontFamily: font.primary_mont,
        fontSize: 13,
    },
    time: {
        position: 'absolute',
        top: 0,
        right: 0,
        fontFamily: font.primary_mont_regular,
        fontSize: 13,
        transform: [{
            translateY: -5
        }]
    },
    fare: {
        position: 'absolute',
        bottom: 0,
        right: 0,
        fontFamily: font.primary_mont_bold,
        fontSize: 16
    }
});

export default BookingDetails;
