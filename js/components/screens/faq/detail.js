import React, { Component } from 'react';
import PropTypes from 'prop-types';
import {
    View,
    StyleSheet,
    Text,
    Dimensions,
    TouchableOpacity,
    TextInput,
    KeyboardAvoidingView,
    ActivityIndicator,
    ScrollView
} from 'react-native';
import * as Animatable from 'react-native-animatable';
import { HamContainer } from '../../common';
import { font, color } from '../../theme';
import { Utils } from '../../widgets';

const { width } = Dimensions.get('window');

const faqs = {
    Bills: [{
        body: 'Aenean eu nisl lacinia, aliquam ipsum vitae, pretium ligula?',
        question: 'Cras suscipit porta ante, sed rhoncus lectus placerat eget.'
    }, {
        body: 'Etiam nunc tortor, feugiat a semper sit amet, pellentesque eu sem?',
        question: 'Cras suscipit porta ante, sed rhoncus lectus placerat eget...'
    }, {
        body: 'Aenean eu nisl lacinia, aliquam ipsum vitae, pretium ligula?',
        question: 'Cras suscipit porta ante, sed rhoncus lectus placerat eget.'
    }, {
        body: 'Etiam nunc tortor, feugiat a semper sit amet, pellentesque eu sem?',
        question: 'Cras suscipit porta ante, sed rhoncus lectus placerat eget...'
    }, {
        body: 'Aenean eu nisl lacinia, aliquam ipsum vitae, pretium ligula?',
        question: 'Cras suscipit porta ante, sed rhoncus lectus placerat eget.'
    }]
};

class FAQDetail extends Component {
    constructor(props) {
        super(props);
        this.state = {
            somethingElse: '',
            submiting: false
        };
        this.submitingBind = this.submit.bind(this);
    }

    submit() {
        if (!this.state.somethingElse.trim()) {
            Utils.shakeAndDisplayToast(this.somethingElseContainer, 'Type in a question to continue.');
            return;
        }
        this.setState({
            submiting: true
        });
        setTimeout(() => {
            this.setState({
                submiting: false,
                somethingElse: ''
            });
        }, 2000);
    }

    render() {
        const {
            heading,
            headerText,
            headingText,
            headingTextLower,
            somethingElse,
            somethingElseLabel,
            somethingElseInput,
            somethingElseSubmit,
            somethingElseSubmitBtn,
            somethingElseSubmitText
        } = styles;
        const { category } = this.props.navigation.state.params;
        return (
            <HamContainer
                title={`${category} - FAQ`}
                backEnabled
                navigate={this.props.navigation.goBack}
            >
                <KeyboardAvoidingView
                    behavior="position"
                    style={{ flex: 1 }}
                    contentContainerStyle={{ flex: 1 }}
                >
                    <View style={heading} >
                        <ScrollView>
                            <Text style={headerText}>
                                {category}
                            </Text>
                            <View style={{ height: width / 40 }} />
                            {faqs.Bills.map(faq => (
                                <View>
                                    <Text style={headingText}>
                                        {faq.body}
                                    </Text>
                                    <Text style={headingTextLower}>
                                        {faq.question}
                                    </Text>
                                    <View style={{ height: width / 40 }} />
                                </View>
                            ))}
                        </ScrollView>
                    </View>
                    <View style={somethingElse} >
                        <View style={{ flex: 0.2 }} />
                        <Text style={somethingElseLabel}>Something else? Ask here.</Text>
                        <View style={{ flex: 0.1 }} />
                        <Animatable.View
                            ref={component => { this.somethingElseContainer = component; }}
                            style={{ flex: 1.2 }}
                        >
                            <TextInput
                                underlineColorAndroid="#00000000"
                                style={somethingElseInput}
                                multiline
                                maxLength={100}
                                numberOfLines={3}
                                placeholder="Enter your question here"
                                value={this.state.somethingElse}
                                onChangeText={value => this.setState({
                                    somethingElse: value
                                })}
                            />
                        </Animatable.View>
                        <View style={{ flex: 0.2 }} />
                        <View style={somethingElseSubmit}>
                            <TouchableOpacity
                                disabled={this.state.submiting}
                                style={somethingElseSubmitBtn}
                                onPress={this.submitingBind}
                            >
                                {
                                    !this.state.submiting ?
                                        <Text style={somethingElseSubmitText}>Submit</Text>
                                        : <ActivityIndicator color="black" />
                                }
                            </TouchableOpacity>
                        </View>
                        <View style={{ flex: 0.2 }} />
                    </View>
                </KeyboardAvoidingView>
            </HamContainer>
        );
    }
}

const styles = StyleSheet.create({
    heading: {
        flex: 1.85,
        marginLeft: width / 15,
        marginRight: width / 15,
        paddingTop: width / 40
    },
    headerText: {
        fontFamily: font.primary_mont_regular,
        fontSize: 16
    },
    headingText: {
        fontFamily: font.primary_mont,
        fontSize: 16
    },
    headingTextLower: {
        fontFamily: font.primary_mont_semi_bold,
        fontSize: 15
    },
    somethingElse: {
        flex: 0.85,
        flexDirection: 'column',
        marginLeft: width / 25,
        marginRight: width / 25
    },
    somethingElseLabel: {
        fontFamily: font.primary_mont_regular,
        fontSize: 14,
        paddingLeft: width / 45
    },
    somethingElseInput: {
        fontFamily: font.primary_mont_regular,
        fontSize: 14,
        flex: 1,
        borderRadius: width / 23,
        borderColor: color.grey,
        borderWidth: 1,
        paddingLeft: width / 25,
        paddingRight: width / 25
    },
    somethingElseSubmitBtn: {
        flex: 1,
        justifyContent: 'center',
        backgroundColor: color.green,
        borderRadius: width / 14,
        borderColor: color.grey,
        borderWidth: 1,
    },
    somethingElseSubmit: {
        flex: 0.75
    },
    somethingElseSubmitText: {
        fontFamily: font.primary_mont_medium,
        fontSize: 17,
        alignSelf: 'center'
    }
});

FAQDetail.propTypes = {
    navigation: PropTypes.shape({
        goBack: PropTypes.func,
        state: PropTypes.shape({
            params: PropTypes.shape({
                category: PropTypes.string
            })
        }),
    }).isRequired
};

export default FAQDetail;
