import React from 'react';
import PropTypes from 'prop-types';
import {
    View,
    StyleSheet,
    Dimensions,
    Text,
    TouchableOpacity,
    Image,
    ScrollView
} from 'react-native';
import { Dialog } from 'react-native-simple-dialogs';
import { color, font } from '../../theme';
import { HamContainer } from '../../common';
import { terms, privacy } from './texts';

const { width } = Dimensions.get('window');

const readOnly = {
    copyright: {
        dialogTitle: 'Copyright Info',
        dialogBody: `Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.`
    },
    privacy: {
        dialogTitle: 'Privacy Statement',
        dialogBody: privacy
    },
    terms: {
        dialogTitle: 'Terms of Service',
        dialogBody: terms
    },
    operations: {
        dialogTitle: 'Operations',
        dialogBody: 'Nam interdum ullamcorper felis viverra mattis. Sed condimentum gravida erat et luctus. Pellentesque habitant morbi tristique senectus et netus et malesuada fames ac turpis egestas. Morbi quis justo at libero tempor vehicula non ac velit. Pellentesque consequat ante mi, eu blandit nunc tristique non. Aliquam consequat risus ligula, ac rutrum lorem facilisis in. Mauris tempor felis odio, nec porta velit pellentesque ut. Praesent iaculis nisl ut porta mollis. Duis auctor mattis eleifend. Donec id bibendum tellus. Vestibulum mollis molestie nibh nec finibus. Ut fermentum semper tempus. Nullam ornare turpis a bibendum molestie. Vestibulum dignissim convallis mi in tincidunt.'
    }
};

class Contact extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            dialogVisible: false,
            dialogTitle: '',
            dialogBody: ''
        };
        this.openDialogBind = this.openDialog.bind(this);
    }

    openDialog(choice) {
        this.setState({
            dialogVisible: true,
            ...readOnly[choice]
        });
    }

    render() {
        const {
            logo,
            body,
            optionContainer,
            option,
            closeText,
            dialogBodyStyle
        } = styles;
        const { dialogTitle, dialogBody, dialogVisible } = this.state;
        const { navigation: { navigate } } = this.props;
        return (
            <HamContainer
                title="Legal"
                navigate={navigate}
            >
                <View style={logo} >
                    <Image style={{ height: width / 4, width: width / 4 }} source={require('../../../../assets/icons/logo_colored.png')} />
                </View>
                <View style={{ flex: 0.3 }} />
                <View style={body} >
                    <TouchableOpacity
                        style={optionContainer}
                        onPress={() => this.openDialog('copyright')}
                    >
                        <Text style={option}>Copyright</Text>
                    </TouchableOpacity>
                    <TouchableOpacity
                        style={optionContainer}
                        onPress={() => this.openDialog('privacy')}
                    >
                        <Text style={option}>Privacy Statement</Text>
                    </TouchableOpacity>
                    <TouchableOpacity
                        style={optionContainer}
                        onPress={() => this.openDialog('terms')}
                    >
                        <Text style={option}>Terms of Service</Text>
                    </TouchableOpacity>
                    <TouchableOpacity
                        style={optionContainer}
                        onPress={() => this.openDialog('operations')}
                    >
                        <Text style={option}>Operations / About</Text>
                    </TouchableOpacity>
                </View>
                <Dialog
                    visible={dialogVisible}
                    title={dialogTitle}
                    onTouchOutside={() => this.setState({ dialogVisible: false })}
                    contentStyle={{ justifyContent: 'center', alignItems: 'center' }}
                    animationType="fade"
                >
                    <View style={{ alignItems: 'center' }}>
                        <Image style={{ height: width / 8, width: width / 8, marginBottom: 20 }} source={require('../../../../assets/icons/logo_colored.png')} />
                        <View style={{ height: width / 1, marginBottom: 20 }}>
                            <ScrollView>
                                <Text style={dialogBodyStyle}>
                                    {dialogBody}
                                </Text>
                            </ScrollView>
                        </View>
                        <TouchableOpacity
                            onPress={() => this.setState({ dialogVisible: false })}
                        >
                            <Text style={closeText}>
                              Close
                            </Text>
                        </TouchableOpacity>
                    </View>
                </Dialog>
            </HamContainer>
        );
    }
}

Contact.propTypes = {
    navigation: PropTypes.shape({
        navigate: PropTypes.func
    }).isRequired
};

const styles = StyleSheet.create({
    logo: {
        flex: 1.3,
        alignItems: 'center',
        justifyContent: 'center'
    },
    body: {
        flex: 4,
        flexDirection: 'column',
        justifyContent: 'flex-start',
        paddingLeft: width / 15,
        paddingRight: width / 15
    },
    optionContainer: {
        paddingBottom: 18
    },
    option: {
        fontFamily: font.primary_mont,
        fontSize: 14
    },
    dialogBodyStyle: {
        color: color.darkgrey
    },
    closeText: {
        fontFamily: font.primary_mont_medium
    }
});

export default Contact;
