import { RAZORPAY_KEY_ID } from '../../../utils/constants';
import { color } from '../../theme';

const getPaymentForm = (amount, name, email, phone) =>
    `<script src="https://checkout.razorpay.com/v1/checkout.js"></script>
    <script>
        var options = {
            "key": "${RAZORPAY_KEY_ID}",
            "amount": "${amount * 100}",
            "name": "Meego",
            "description": "Payment for Meego",
            "image": "https://www.dropbox.com/s/h5cut49srux8r6c/512.png?dl=1",
            "handler": function (response) {
                window.postMessage(response.razorpay_payment_id);
            },
            "prefill": {
                "name": "${name}",
                "email": "${email}",
                "contact": "${phone}"
            },
            "theme": {
                "color": "${color.pink}"
            },
            "modal": {
                "ondismiss": function() {
                  window.postMessage(null);
                }
            }
        };
        var rzp1 = new Razorpay(options);
        window.onload = function() {
            rzp1.open();
        }
    </script>
    `;

export default getPaymentForm;
