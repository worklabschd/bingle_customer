import React, { Component } from 'react';
import {
    Image,
    StatusBar,
    Platform,
    View,
    StyleSheet
} from 'react-native';
import { AppLoading, Asset, Font, Constants, Location, Permissions } from 'expo';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import Screens from './screens';
import Logger from '../utils/logging';
import Events from '../utils/events';
import { setLocationPermission, setLocation } from '../actions';
import { reHydratedStore } from '../reducers';
import { StatsAPI } from '../api';

const cacheImages = images => images.map(image => {
    if (typeof image === 'string') {
        return Image.prefetch(image);
    }
    return Asset.fromModule(image).downloadAsync();
});

const cacheFonts = fonts => fonts.map(font => Font.loadAsync(font));

// Get route name from navigation state.
const getRouteName = navigationState => {
    if (!navigationState) {
        return null;
    }
    const route = navigationState.routes[navigationState.index];
    // dive into nested navigators.
    if (route.routes) {
        return getRouteName(route);
    }
    return route.routeName;
};

const onScreenChange = (previousState, currentState) => {
    const currentScreen = getRouteName(currentState);
    const previousScreen = getRouteName(previousState);

    if (previousScreen !== currentScreen && !currentScreen.includes('Drawer')) {
        // If the user has changed his screen and its not a drawer toggle.
        Events.trackScreen(currentScreen, { previousScreen });
    }
};

class Meego extends Component {
    constructor(props) {
        super(props);
        this.state = {
            loading: true
        };
        StatusBar.setBarStyle('dark-content', true);
        this.loadMeegoBind = this.loadMeego.bind(this);
    }

    async getLocation(permissionGranted) {
        const { locationServicesEnabled, gpsAvailable } = await Location.getProviderStatusAsync();
        const { coords: { latitude, longitude } } = await Location.getCurrentPositionAsync({
            enableHighAccuracy: permissionGranted && locationServicesEnabled && (Platform.OS === 'ios' || gpsAvailable),
            maximumAge: 200000
        });
        this.props.setLocation(latitude, longitude);
    }

    async askPermissions() {
        let granted = true;
        if (!(Platform.OS === 'android' && !Constants.isDevice)) {
            const { status } = await Permissions.askAsync(Permissions.LOCATION);
            granted = status === 'granted';
        }
        this.props.setLocationPermission(granted);
        return granted;
    }

    async loadMeego() {
        try {
            // Firing up sentry & segment.
            Logger.init();
            Events.init();
        } catch (err) {
            console.warn(err);
        }
        const imageAssets = cacheImages([
            require('../../assets/icons/white_logo.png'),
            require('../../assets/icons/call_button.png'),
            require('../../assets/icons/compass_icon.png'),
            require('../../assets/icons/envelope_white.png'),
            require('../../assets/icons/language.png'),
            require('../../assets/icons/left_arrow.png'),
            require('../../assets/icons/location_icon.png'),
            require('../../assets/icons/long_arrow.png'),
            require('../../assets/icons/own_location.png'),
            require('../../assets/icons/rupee.png'),
            require('../../assets/icons/telephone.png'),
            require('../../assets/icons/unlocked_white.png'),
            require('../../assets/icons/logo_colored.png'),
            require('../../assets/icons/avatar.png')
        ]);

        const fontAssets = cacheFonts([
            { avenir_regular: require('../../assets/fonts/avenir-next-regular.ttf') },
            { mont_regular: require('../../assets/fonts/montserrat-regular.ttf') },
            { mont_light: require('../../assets/fonts/montserrat-light.ttf') },
            { mont_medium: require('../../assets/fonts/montserrat-medium.ttf') },
            { mont_bold: require('../../assets/fonts/montserrat-bold.ttf') },
            { mont_semi_bold: require('../../assets/fonts/montserrat-semibold.ttf') }
        ]);
        // Ask android permission for Location.
        const granted = await this.askPermissions();
        // Get location with or without GPS depending upon status of permission.
        await this.getLocation(granted);

        // The app is now going to be opened.
        // Track this event in Segment and inside our own server.
        Meego.sendAppOpenEvent();

        // Resolve other assets & redux store rehydration.
        await Promise.all([...imageAssets, ...fontAssets, reHydratedStore]);
    }

    static sendAppOpenEvent() {
        Events.track('App Opened.');
        StatsAPI.appOpened();
    }

    render() {
        return (
            this.state.loading ?
                <AppLoading
                    startAsync={this.loadMeegoBind}
                    onFinish={() => this.setState({ loading: false })}
                    onError={console.warn}
                />
                :
                <View style={styles.app}>
                    <Screens
                        onNavigationStateChange={onScreenChange}
                    />
                </View>
        );
    }
}

Meego.propTypes = {
    setLocationPermission: PropTypes.func.isRequired,
    setLocation: PropTypes.func.isRequired
};

const mapActionsToProps = {
    setLocationPermission,
    setLocation
};

const styles = StyleSheet.create({
    app: {
        position: 'absolute',
        top: 0,
        bottom: 0,
        left: 0,
        right: 0
    }
});

export default connect(({ location }) => ({ location }), mapActionsToProps)(Meego);
