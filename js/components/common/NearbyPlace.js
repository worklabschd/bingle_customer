import React from 'react';
import PropTypes from 'prop-types';
import {
    View,
    Dimensions,
    StyleSheet,
    Text,
    TouchableOpacity,
    TouchableWithoutFeedback,
    Image,
    Platform
} from 'react-native';
import { color, font } from '../theme';
import { BASE_PATH } from '../../utils/constants';

const { width } = Dimensions.get('window');

const NearbyPlace = ({
    name, distance, imagePath, onExplore
}) => {
    const {
        container,
        imageContainer,
        nameContainer,
        nameStyle,
        infoStyle,
        infoContainerStyle,
        bodyContainer,
        distanceContainer,
        distanceStyle
    } = styles;
    return (
        <TouchableWithoutFeedback onPress={() => {}}>
            <View style={container} >
                <View style={imageContainer}>
                    <Image
                        source={{ uri: `${BASE_PATH}${imagePath}` }}
                        style={{
                            height: null,
                            width: null,
                            flex: 1,
                            borderTopLeftRadius: Platform.OS === 'android' ? width / 18 : 0,
                            borderBottomLeftRadius: Platform.OS === 'android' ? width / 18 : 0,
                        }}
                    />
                </View>
                <View style={bodyContainer}>
                    <View style={nameContainer}>
                        <Text numberOfLines={1} style={nameStyle}>{name}</Text>
                        <View style={{ flex: 0.6 }} />
                        <View style={{ flex: 1.3, flexDirection: 'row' }}>
                            <TouchableOpacity onPress={onExplore} style={infoContainerStyle}>
                                <Text style={infoStyle}>More Info</Text>
                            </TouchableOpacity>
                            <View style={{ flex: 0.3 }} />
                        </View>
                    </View>
                    <View style={distanceContainer}>
                        <Text style={distanceStyle}>{distance}</Text>
                    </View>
                </View>
            </View>
        </TouchableWithoutFeedback>
    );
};

NearbyPlace.propTypes = {
    name: PropTypes.string.isRequired,
    distance: PropTypes.string.isRequired,
    imagePath: PropTypes.string.isRequired,
    onExplore: PropTypes.func.isRequired
};

const styles = StyleSheet.create({
    container: {
        height: width / 5,
        borderRadius: width / 18,
        marginBottom: 10,
        backgroundColor: color.lightGrey,
        borderColor: color.grey,
        borderWidth: 0.5,
        flexDirection: 'row'
    },
    imageContainer: {
        flex: 1.65,
        borderTopLeftRadius: width / 18,
        borderBottomLeftRadius: width / 18,
        borderRightWidth: 0.5,
        overflow: 'hidden',
        borderColor: color.grey
    },
    bodyContainer: {
        flex: 3,
        flexDirection: 'row',
        padding: 6.5,
        paddingLeft: 10
    },
    nameContainer: {
        flex: 1.1,
        flexDirection: 'column'
    },
    nameStyle: {
        fontFamily: font.primary_mont_medium,
        fontSize: 14
    },
    infoContainerStyle: {
        backgroundColor: 'white',
        flex: 1.3,
        borderRadius: width / 10,
        alignItems: 'center',
        justifyContent: 'center',
        borderWidth: 0.5,
        borderColor: color.grey
    },
    infoStyle: {
        fontFamily: font.primary_mont,
        fontSize: 12,
        backgroundColor: 'transparent'
    },
    distanceContainer: {
        flex: 0.6,
        alignItems: 'flex-end',
        paddingRight: 6
    },
    distanceStyle: {
        flex: 1,
        backgroundColor: 'transparent',
        fontFamily: font.primary_mont_medium,
        fontSize: 14
    }
});

export default NearbyPlace;
