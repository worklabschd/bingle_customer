import React from 'react';
import PropTypes from 'prop-types';
import AwesomeAlert from 'react-native-awesome-alerts';
import { color } from '../theme';

const MeegoDialog = ({
    title,
    body,
    visible,
    onClose,
    closeText,
    cancelText,
    dismissOnOutsideClick,
    showCancel,
    showConfirm,
    onCancel,
    showProgress
}) => (
    <AwesomeAlert
        showProgress={showProgress}
        show={visible}
        title={title}
        message={body}
        showConfirmButton={showConfirm}
        showCancelButton={showCancel}
        closeOnTouchOutside={dismissOnOutsideClick}
        cancelText={cancelText}
        confirmText={closeText}
        confirmButtonColor={color.maroon}
        onCancelPressed={onCancel}
        onConfirmPressed={onClose}
    />
);

MeegoDialog.propTypes = {
    title: PropTypes.string.isRequired,
    body: PropTypes.string,
    visible: PropTypes.bool.isRequired,
    onClose: PropTypes.func,
    closeText: PropTypes.string,
    cancelText: PropTypes.string,
    dismissOnOutsideClick: PropTypes.bool,
    showCancel: PropTypes.bool,
    showConfirm: PropTypes.bool,
    onCancel: PropTypes.func,
    showProgress: PropTypes.bool
};

MeegoDialog.defaultProps = {
    dismissOnOutsideClick: false,
    body: undefined,
    onClose: () => {},
    showCancel: false,
    showConfirm: true,
    closeText: 'Okay',
    cancelText: 'Cancel',
    onCancel: () => {},
    showProgress: false
};

export default MeegoDialog;
