import React from 'react';
import PropTypes from 'prop-types';
import { LinearGradient } from 'expo';
import { StyleSheet, StatusBar, Platform } from 'react-native';
import { color } from '../theme';

const MeegoBackground = (props) => (
    <LinearGradient
        start={{ x: 1, y: 0.2 }}
        end={{ x: 0, y: 1 }}
        locations={[0, 0.55, 1]}
        colors={[color.maroon, color.pink, color.yellow]}
        style={styles.container}
    >
        <StatusBar
            backgroundColor={Platform.OS === 'android' ? color.maroon : 'transparent'}
            barStyle="light-content"
        />
        {props.children}
    </LinearGradient>
);

MeegoBackground.propTypes = {
    children: PropTypes.oneOfType([
        PropTypes.element,
        PropTypes.array
    ]).isRequired
};

const styles = StyleSheet.create({
    container: {
        flex: 1,
        flexDirection: 'column'
    }
});

export default MeegoBackground;
