const shake = animatableView => {
    if ('shake' in animatableView) {
        animatableView.shake(1300);
        return;
    }
    console.warn('View does not have shake function. Possibly not of type Animatable?');
};

export { shake }; // eslint-disable-line
