import * as Snackbar from './snackbar';
import * as MeegoAnimation from './meego_animation';

const Utils = {
    shakeAndDisplayToast: (animatableView, message) => {
        MeegoAnimation.shake(animatableView);
        Snackbar.displayToast(message);
    }
};

export { Snackbar, MeegoAnimation, Utils };
