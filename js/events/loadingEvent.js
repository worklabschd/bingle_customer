import { Observable } from 'rxjs';

function LoadingEvent() {
    this.observable = Observable.create(observer => {
        this.observer = observer;
    });
    this.subscription = null;
}

LoadingEvent.prototype.subscribe = function (cb) {
    this.subscription = this.observable.subscribe(cb);
};

LoadingEvent.prototype.show = function () {
    this.observer.next(true);
};

LoadingEvent.prototype.cancel = function () {
    this.observer.next(false);
};

LoadingEvent.prototype.dispose = function () {
    this.subscription.unsubscribe();
};

export default new LoadingEvent();
