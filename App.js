import React from 'react';
import MeegoHOC from './js/components/hoc';
import Meego from './js/components';

export default () => <MeegoHOC><Meego /></MeegoHOC>;
